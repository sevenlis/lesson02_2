package ru.clevertec.sevenlis.android2022;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.OnRecyclerViewInteractionListener {

    public MainActivity() {
        super(R.layout.activity_main);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setReorderingAllowed(true)
                    .disallowAddToBackStack()
                    .replace(R.id.fragment_container_view, MainFragment.class, null, MainFragment.class.getSimpleName())
                    .commit();
        }
    }

    @Override
    public void onListItemSelected(Item item, View itemView) {
        final ImageView imageView = itemView.findViewById(R.id.imageView);
        final TextView tvTitle = itemView.findViewById(R.id.title);
        final TextView tvDescription = itemView.findViewById(R.id.description);

        Bundle args = new Bundle();
        args.putString("title", item.getTitle());
        args.putString("description", item.getDescription());
        args.putString("tn_imageView", ViewCompat.getTransitionName(imageView));
        args.putString("tn_tvTitle", ViewCompat.getTransitionName(tvTitle));
        args.putString("tn_tvDescription", ViewCompat.getTransitionName(tvDescription));

        getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(imageView, args.getString("tn_imageView"))
                .addSharedElement(tvTitle, args.getString("tn_tvTitle"))
                .addSharedElement(tvDescription, args.getString("tn_tvDescription"))
                .setReorderingAllowed(true)
                .addToBackStack(DetailFragment.class.getSimpleName())
                .replace(R.id.fragment_container_view, DetailFragment.class, args, DetailFragment.class.getSimpleName())
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}